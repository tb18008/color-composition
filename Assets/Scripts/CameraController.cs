using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 worldOrigin = new Vector3(0.0f,0.5f,0.0f);

    public float rotationSpeed = 10;
    public int rotationCeiling = 90;
    public int rotationFloor = 0;

    private Vector3 _vectorToCamera;
    private Vector3 _projection;
    
    private float _productOfVectors;
    private float _cosValue;
    private float _angle;
    private float _calculatedAngle;
    // Start is called before the first frame update
    void Start()
    {
        CalculateVectors();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetMouseButton(0)) return;   //Rotating only when left mouse button is down
        
        //Rotation around Y axis (not restricted)
        transform.RotateAround(worldOrigin, Vector3.up, rotationSpeed * Input.GetAxis ("Mouse X"));

        //Rotation around X axis (not restricted)
        CalculateVectors();
        
        //Get angle between 'floor' and camera
        _productOfVectors = Vector3.Dot(_vectorToCamera, _projection);
        _cosValue = _productOfVectors / _vectorToCamera.magnitude * _projection.magnitude;
        _angle = Mathf.Acos(_cosValue) * 180 / Mathf.PI;  //Convert radians to degrees

        var rotationY = - Input.GetAxis ("Mouse Y");    //Get the amount the mouse has moved (inverted)
            
        _calculatedAngle = _angle + rotationSpeed * rotationY;    //What the angle would be if the rotation was executed

        if (_calculatedAngle <= rotationCeiling 
            && _calculatedAngle >= rotationFloor)  //Clamp the rotation between angles
        {
            //Execute rotation around the cross vector
            transform.RotateAround(worldOrigin, Vector3.Cross(_vectorToCamera, Vector3.up), rotationSpeed * rotationY);
        }
    }

    private void CalculateVectors()
    {
        _vectorToCamera = transform.position - worldOrigin; //Vector from world origin to camera
        _vectorToCamera.Normalize();
        _projection = new Vector3(_vectorToCamera.x,0,_vectorToCamera.z);   //Vector projected on the 'floor'
        _projection.Normalize();
    }

}
