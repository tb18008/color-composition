using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResourceManager
{
    public static Texture2D[] Textures = new Texture2D[4];

    // public static Texture2D[] ImageData
    // {
    //     get { return _imageData; }
    //     set
    //     {
    //         var texture = value;
    //         _imageData[0] = texture; 
    //     }
    // }

    public static void SetTextures(Texture2D rgbaTexture)
    {
        Textures[0] = rgbaTexture;  //RGBA
        Textures[1] = new Texture2D(rgbaTexture.width, rgbaTexture.height); //R
        Textures[2] = new Texture2D(rgbaTexture.width, rgbaTexture.height); //G
        Textures[3] = new Texture2D(rgbaTexture.width, rgbaTexture.height); //B
        for (var i = 0; i < rgbaTexture.height; i++)
        {
            for (var j = 0; j < rgbaTexture.width; j++)
            {
                Textures[1].SetPixel(i,j,new Color(rgbaTexture.GetPixel(i,j).r,0,0)); //R
                Textures[2].SetPixel(i,j,new Color(0,rgbaTexture.GetPixel(i,j).g,0)); //G
                Textures[3].SetPixel(i,j,new Color(0,0,rgbaTexture.GetPixel(i,j).b)); //B
            }
        }
        
        Textures[1].Apply();
        Textures[2].Apply();
        Textures[3].Apply();
    }
}
