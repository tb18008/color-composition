using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectFileDialog : MonoBehaviour
{
    
    private InputField _inputField;
    private Texture2D _imageData;
    
    // Start is called before the first frame update
    void Start()
    {
        //Set default image path in the input field text
        _inputField = GetComponent<InputField>();
        _inputField.text = Application.dataPath + "/Resources/Gradients.png";
    }
    
    
    public void ReadFile()
    {
        //Set default image path in the input field text
        _inputField = GetComponent<InputField>();
        var bytes = File.ReadAllBytes(_inputField.text);
        _imageData = new Texture2D(1, 1);
 
        _imageData.LoadImage(bytes);

        if (_imageData.width > 512 || _imageData.height > 512) return;

        ResourceManager.SetTextures(_imageData);
        
        SceneManager.LoadScene("MeshPreview");
    }
}
