using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructMesh : MonoBehaviour
{
    private static Texture2D[] _textures = new Texture2D[4];
    private Texture2D _currentTexture;
    
    private int _width = 1;
    private int _height = 1;

    private Vector3[] _vertices;
    private int[] _triangles;
    private Vector3[] _normals;
    private Vector2[] _uv;


    private float _scaleProportion;

    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;
    
    // Start is called before the first frame update
    void Start()
    {
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();
        _meshFilter = gameObject.AddComponent<MeshFilter>();
        
        ColorComponents(0);
        ScaleToFit();
        AlignToWorldOrigin();
    }

    public void ColorComponents(int channel)
    {
        
        //Get width and height of image
        _textures = ResourceManager.Textures;
        _currentTexture = _textures[channel];

        _currentTexture.filterMode = FilterMode.Point;
        if (_textures != null)
        {
            _width = _currentTexture.width;
            _height = _currentTexture.height;
        }

        GenerateMesh();
    }

    private void GenerateMesh()
    {
        var imageTexture = new Material(Shader.Find("Unlit/Texture")) {mainTexture = _currentTexture};
        _meshRenderer.sharedMaterial = imageTexture;
        var mesh = new Mesh();
        
        
        SetScaleProportion();
        PositionVertices();
        mesh.vertices = _vertices;

        ConnectTriangles();
        mesh.triangles = _triangles;

        CalculateNormals();
        mesh.normals = _normals;

        MapUV();
        mesh.uv = _uv;
        
        _meshFilter.mesh = mesh;
    }
    private void ConnectTriangles()
    {
        
        _triangles = new int[_vertices.Length * 6];
        for (var i = 0; i < _height * (_width-1) - 1; i++)
        {
            if ((i + 1) % _height == 0) continue;
            _triangles[i * 6] = i;
            _triangles[i * 6 + 1] = i + 1;
            _triangles[i * 6 + 2] = i + _height + 1;
            _triangles[i * 6 + 3] = i;
            _triangles[i * 6 + 4] = i + _height + 1;
            _triangles[i * 6 + 5] = i + _height;

        }
    }
    private void PositionVertices()
    {
        _vertices = new Vector3[_width * _height];

        for (var i = 0; i < _width; i++)
        {
            for (var j = 0; j < _height; j++)
            {
                var pixelValue =
                    (_currentTexture.GetPixel(i, j).r + _currentTexture.GetPixel(i, j).g + _currentTexture.GetPixel(i, j).b) / 3;

                _vertices[i * _height + j] = new Vector3(i,pixelValue / _scaleProportion,j);
            }
        }
    }
    private void CalculateNormals()
    {
        _normals = new Vector3[_vertices.Length];
        //
        Vector3 normal;
                
        Vector3[] surroundingNormals = new Vector3[9];
        
        for (int i = 0; i < _vertices.Length; i++)
        {
            /*
                 *  0 1 2
                 *  3 4 5
                 *  6 7 8
                 */
                
                int[] surrounding = new int[9];
                surrounding[0] = i - _width - 1;
                surrounding[1] = i - _width;
                surrounding[2] = i - _width + 1;
                
                surrounding[3] = i - 1;
                surrounding[4] = i;
                surrounding[5] = i + 1;
                
                surrounding[6] = i + _width - 1;
                surrounding[7] = i + _width;
                surrounding[8] = i + _width + 1;
                
                if (i == 0) //Top left
                {
                    surroundingNormals[5] = Vector3.Cross(
                        _vertices[surrounding[5]] - _vertices[surrounding[4]],
                        _vertices[surrounding[8]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[7] = Vector3.Cross(
                        _vertices[surrounding[8]] - _vertices[surrounding[4]],
                        _vertices[surrounding[7]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[5] + surroundingNormals[7];
                }
                else if (i == _width - 1) //Top right
                {
                    surroundingNormals[7] = Vector3.Cross(
                        _vertices[surrounding[7]] - _vertices[surrounding[4]],
                        _vertices[surrounding[6]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[3] = Vector3.Cross(
                        _vertices[surrounding[6]] - _vertices[surrounding[4]],
                        _vertices[surrounding[3]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[7] + surroundingNormals[3];
                }
                else if (i == _vertices.Length - _width) //Bottom left
                {
                    surroundingNormals[1] = Vector3.Cross(
                        _vertices[surrounding[1]] - _vertices[surrounding[4]],
                        _vertices[surrounding[2]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[5] = Vector3.Cross(
                        _vertices[surrounding[2]] - _vertices[surrounding[4]],
                        _vertices[surrounding[5]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[1] + surroundingNormals[5];
                }
                else if (i == _vertices.Length - 1) //Bottom right
                {
                    surroundingNormals[3] = Vector3.Cross(
                        _vertices[surrounding[3]] - _vertices[surrounding[4]],
                        _vertices[surrounding[0]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[1] = Vector3.Cross(
                        _vertices[surrounding[0]] - _vertices[surrounding[4]],
                        _vertices[surrounding[1]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[3] + surroundingNormals[1];
                }
                else if (i<_width) //First row
                {
                    surroundingNormals[6] = Vector3.Cross(
                        _vertices[surrounding[7]] - _vertices[surrounding[4]],
                        _vertices[surrounding[6]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[7] = Vector3.Cross(
                        _vertices[surrounding[8]] - _vertices[surrounding[4]],
                        _vertices[surrounding[7]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[6] + surroundingNormals[7];
                }
                else if(i > _vertices.Length - _width - 1)    //Last row
                {
                    surroundingNormals[0] = Vector3.Cross(
                        _vertices[surrounding[0]] - _vertices[surrounding[4]],
                        _vertices[surrounding[1]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[2] = Vector3.Cross(
                        _vertices[surrounding[1]] - _vertices[surrounding[4]],
                        _vertices[surrounding[2]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[0] + surroundingNormals[2];
                }
                else if(i % _width == 0 && i != _vertices.Length - _width)  //Left column
                {
                    surroundingNormals[2] = Vector3.Cross(
                        _vertices[surrounding[2]] - _vertices[surrounding[4]],
                        _vertices[surrounding[5]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[8] = Vector3.Cross(
                        _vertices[surrounding[5]] - _vertices[surrounding[4]],
                        _vertices[surrounding[8]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[2] + surroundingNormals[8];
                }
                else if(i % _width == _width - 1 && i != _vertices.Length - 1)   //Right column
                {
                    surroundingNormals[0] = Vector3.Cross(
                        _vertices[surrounding[6]] - _vertices[surrounding[4]],
                        _vertices[surrounding[3]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[6] = Vector3.Cross(
                        _vertices[surrounding[3]] - _vertices[surrounding[4]],
                        _vertices[surrounding[0]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[0] + surroundingNormals[6];
                }
                else   //Middle
                {
                    
                    surroundingNormals[1] = Vector3.Cross(
                        _vertices[surrounding[1]] - _vertices[surrounding[4]],
                        _vertices[surrounding[2]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[2] = Vector3.Cross(
                        _vertices[surrounding[2]] - _vertices[surrounding[4]],
                        _vertices[surrounding[5]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[5] = Vector3.Cross(
                        _vertices[surrounding[5]] - _vertices[surrounding[4]],
                        _vertices[surrounding[8]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[8] = Vector3.Cross(
                        _vertices[surrounding[8]] - _vertices[surrounding[4]],
                        _vertices[surrounding[7]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[7] = Vector3.Cross(
                        _vertices[surrounding[7]] - _vertices[surrounding[4]],
                        _vertices[surrounding[6]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[6] = Vector3.Cross(
                        _vertices[surrounding[6]] - _vertices[surrounding[4]],
                        _vertices[surrounding[3]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[3] = Vector3.Cross(
                        _vertices[surrounding[3]] - _vertices[surrounding[4]],
                        _vertices[surrounding[0]] - _vertices[surrounding[4]]);
                    
                    surroundingNormals[0] = Vector3.Cross(
                        _vertices[surrounding[0]] - _vertices[surrounding[4]],
                        _vertices[surrounding[1]] - _vertices[surrounding[4]]);
                    
                    normal = surroundingNormals[1] + 
                             surroundingNormals[2] + 
                             surroundingNormals[5] + 
                             surroundingNormals[8] + 
                             surroundingNormals[7] + 
                             surroundingNormals[6] + 
                             surroundingNormals[3] + 
                             surroundingNormals[0];
                }
                
                normal.Normalize();
                _normals[i] = normal;
        }
    }
    private void MapUV()
    {
        _uv = new Vector2[_vertices.Length];
        for (int i = 0; i < _width; i++)
        {
            for (int j = 0; j < _height; j++)
            {
                _uv[i * _height + j] = new Vector2((float)i / (_width - 1), (float)j / (_height - 1));
            }
        }
    }
    private void SetScaleProportion()
    {
        var longestSide = _height;
        if (_width > _height) longestSide = _width;

        _scaleProportion = (float)1 / longestSide;
    }

    private void ScaleToFit()
    {
        gameObject.transform.localScale = Vector3.Scale(gameObject.transform.localScale, new Vector3(_scaleProportion, +_scaleProportion, _scaleProportion));
    }

    private void AlignToWorldOrigin()
    {
        float centerX = _width * _scaleProportion / 2;
        float centerZ = _height * _scaleProportion / 2;
        
        gameObject.transform.Translate(new Vector3(- centerX, 0, -centerZ));
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
